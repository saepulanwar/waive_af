Dependencies:
1. BalaReva.Excel.Activities = 2020.0.0
2. EnumerableSplitHelper = 1.0.3
3. UiPath.Credential.Activities = 1.1.6479.13204
4. UiPath.Excel.Aactivities = 1.2.6256.15131
5. UiPath.Mail.Activities = 1.9.0-preview
6. UiPath.System.Activities = 20.6.1-preview
7. UiPath.Terminal.Activities = 1.3.4
8. UiPath.UIAutomation.Activities = 20.6.0-preview
9. UiPath.Word.Activities = 1.5.0-preview